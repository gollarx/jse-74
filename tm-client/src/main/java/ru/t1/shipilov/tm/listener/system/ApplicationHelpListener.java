package ru.t1.shipilov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.api.model.IListener;
import ru.t1.shipilov.tm.event.ConsoleEvent;
import ru.t1.shipilov.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String DESCRIPTION = "Show command list.";

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@NotNull final IListener command : listeners)
            System.out.println(command.getName() + " : " + command.getDescription());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
