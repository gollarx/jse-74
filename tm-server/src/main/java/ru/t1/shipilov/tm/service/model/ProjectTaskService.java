package ru.t1.shipilov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shipilov.tm.repository.model.IProjectRepository;
import ru.t1.shipilov.tm.repository.model.ITaskRepository;
import ru.t1.shipilov.tm.api.service.model.IProjectTaskService;
import ru.t1.shipilov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shipilov.tm.exception.entity.TaskNotFoundException;
import ru.t1.shipilov.tm.exception.entity.field.IndexIncorrectException;
import ru.t1.shipilov.tm.exception.entity.field.ProjectIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.TaskIdEmptyException;
import ru.t1.shipilov.tm.exception.entity.field.UserIdEmptyException;
import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.model.Task;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        @Nullable final Project project;
        project = projectRepository.getOneByUserIdAndId(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task = taskRepository.getOneByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<Task> tasks;
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        tasks = taskRepository.findAllByUserIdAndProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.deleteByUserIdAndId(userId, task.getId());
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer projectIndex) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 1) throw new IndexIncorrectException();
        @NotNull final List<Task> tasks;
        @Nullable Project project;
        @Nullable final List<Project> projectList;
        final Pageable pageable = PageRequest.of(projectIndex - 1, 1);
        projectList = projectRepository.getOneByIndexAndUserId(userId, pageable);
        if (projectList == null || projectList.isEmpty()) throw new ProjectNotFoundException();
        project = projectList.get(0);
        tasks = taskRepository.findAllByUserIdAndProjectId(userId, project.getId());
        for (final Task task : tasks) taskRepository.deleteByUserIdAndId(userId, task.getId());
        projectRepository.deleteByUserIdAndId(userId, project.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String projectId,
                                      @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        if (!projectRepository.existsByUserIdAndId(userId, projectId)) throw new ProjectNotFoundException();
        task = taskRepository.getOneByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskRepository.save(task);
    }

}
