package ru.t1.shipilov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoService {

    void save(@Nullable final String userId, @Nullable final ProjectDTO project);

    void saveAll(@Nullable final String userId, @Nullable final Collection<ProjectDTO> projects);

    void removeAll(@Nullable final String userId);

    void removeAll(@Nullable final String userId, @Nullable Collection<ProjectDTO> projects);

    void removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeOne(@Nullable final String userId, @Nullable final ProjectDTO project);

    @NotNull
    List<ProjectDTO> findAll(@Nullable final String userId);

    @Nullable
    ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    String getProjectNameById(@Nullable final String userId, @Nullable final String id);

}
